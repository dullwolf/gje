package dto;

import java.util.Map;


public class EventDTO {

    private Map<String,String[]> eventMap;

    public Map<String, String[]> getEventMap() {
        return eventMap;
    }

    public void setEventMap(Map<String, String[]> eventMap) {
        this.eventMap = eventMap;
    }
}
