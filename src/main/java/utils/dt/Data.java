package utils.dt;

import java.util.HashMap;

/**
 * 样本类
 * @author dullwolf
 *
 */
public class Data implements Cloneable{
    /**
     * K是特征描述，V是特征值
     */
    private HashMap<String,String> feature = new HashMap<String, String>();

    /**
     * 该样本结论
     */
    private String result;

    public Data(HashMap<String,String> feature,String result){
        this.feature = feature;
        this.result = result;
    }

    public HashMap<String, String> getFeature() {
        return feature;
    }

    public String getResult() {
        return result;
    }

    private void setFeature(HashMap<String, String> feature) {
        this.feature = feature;
    }

    @Override
    public Data clone()
    {
        Data object=null;
        try {
            object = (Data) super.clone();
            object.setFeature((HashMap<String, String>) this.feature.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return object;
    }
}