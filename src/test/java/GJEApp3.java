import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import dto.EventDTO;
import utils.JsonUtils;
import utils.bfs.Graph;
import utils.dt.Data;
import utils.dt.DecisionTree;

import java.util.*;

/**
 * 测试 GJE算法
 *
 * @author dullwolf
 */
public class GJEApp3 {

    /**
     * 每个路线对应女主的CG事件
     */
    private static Map<String, String> map = new HashMap<>();

    /**
     * 每个女主路线对应关键的CG事件个数统计
     */
    private static LinkedHashMap<String, Integer> keyMap = new LinkedHashMap<>();

    /**
     * 所有选项集合
     */
    private static LinkedList<String[]> list = new LinkedList<>();

    /**
     * 分支标签
     */
    private static String[] labels = new String[]{"分支1", "分支2", "分支3", "分支4", "分支5", "分支6", "分支7", "分支8"};

    /**
     * 结局路线
     */
    private static String play[] = {"#", "BAD", "新仓", "叶恵", "乃华", "美由纪"};

    /**
     * 所有的结局路线
     */
    private static List<String> ends = new ArrayList<>();

    //A1 = "教室";
    //B1 = "中庭";
    //C1 = "天台";

    //A2 = "老实同意";
    //B2 = "让她先别等等";
    //C2 = "保持沉默";

    //A3 = "诶，好的";
    //B3 = "都这个时候还能做什么";

    //A4 = "你平时都在这吃午饭";
    //B4 = "什么事都没有";
    //C4 = "美由纪，今天是轮到你来着";

    //A5 = "约新仓";
    //B5 = "自己吃";

    //A6 = "直接回家";
    //B6 = "去协力会";

    //A7 = "去找乃华";
    //B7 = "去找美由纪";
    //C7 = "去找新仓";

    //A8 = "去追叶恵";
    //B8 = "找新仓谈话";
    //C8 = "不知道如何是好";

    public static void main(String[] args) {
        //初始化根节点
        Graph theGraph = new Graph();
        String[] opArr = new String[]{"#"};
        list.add(opArr);
        //初始化事件CG分支
        for (String label : labels) {
            cg(label);
        }
        //获取所有事件节点
        getKeyMap();
        //根节点以及中间节点
        for (int i = 0; i < list.size(); i++) {
            if (i + 1 == list.size()) {
                break;
            }
            String[] a1 = list.get(i);
            String[] a2 = list.get(i + 1);
            addEdge(theGraph, a1, a2);
        }
        String[] last = list.getLast();
        //叶子节点
        for (String aLast : last) {
            theGraph.addVertex(aLast);
        }
        StringBuilder sb = new StringBuilder();
        List<List<String>> allPathList = theGraph.mst();
        for (List<String> onePath : allPathList) {
            if (sb.length() > 0) {
                sb.delete(0, sb.length());
            }
            for (String node : onePath) {
                sb.append(node).append(",");
            }
            String options = sb.substring(2, sb.length() - 1);
            Map<String, Integer> endMap = getTotalEnd(options, map);
            String end = "#";
            // 唯一计划：走筹齐满事件的路线,出现相同的看优先路线
            Set<Map.Entry<String, Integer>> entries = keyMap.entrySet();
            for (Map.Entry<String, Integer> entry : entries) {
                Integer endCount = endMap.get(entry.getKey());
                if (entry.getValue().equals(endCount)) {
                    end = entry.getKey();
                    break;
                }
            }
            if ("#".equals(end)) {
                //还是 # 说明事件没有凑齐,走优先路线 or badEnd
                end = endMap.entrySet().iterator().next().getKey();
            }
            ends.add(options + "," + end);
        }
        DecisionTree decisionTree = new DecisionTree();
        //使用测试样本生成决策树
        DecisionTree.TreeNode tree = decisionTree.createTree(createDataList());
        //展示决策树
        String mapString = tree.showTreeJsonStr();
        System.out.println(mapString);
        String treeJson = getTreeJson(mapString);
        System.out.println("最终结果：" + treeJson);
    }

    /**
     * 添加节点
     *
     */
    private static void addEdge(Graph theGraph, String[] a1, String[] a2) {
        for (String root : a1) {
            //添加叶子根
            theGraph.addVertex(root);
            //再给叶子根添加子节点
            for (String anA2 : a2) {
                theGraph.addEdge(root, anA2);
            }

        }
    }

    /**
     * 递归展示非true的节点，并把结果返回
     *
     * @param mapString json字符串
     */
    private static String getTreeJson(String mapString) {
        List<String> result = Lists.newArrayList();
        JSONObject object = JSONObject.parseObject(mapString);
        StringBuilder sb = new StringBuilder();
        for (String label : labels) {
            if (object.containsKey(label)) {
                JSONObject opJson = object.getJSONObject(label);
                Set<String> cg = opJson.keySet();
                for (String c : cg) {
                    JSONObject node = opJson.getJSONObject(c);
                    if (!node.getBoolean("final")) {
                        sb.append(c).append(",");
                        JSONObject decisionMap = node.getJSONObject("featureDecisionMap");
                        showDecisionMap(decisionMap, sb, result);
                    }
                }
            }
        }
        //下一步重新把集合中的结果构建新的json返回给前端
        for (String str : result) {
            System.out.println(str);
        }
        List<EventDTO> eventDTOList = Lists.newArrayList();
        Map<String, String[]> map;
        EventDTO event;
        for (String[] op : list) {
            event = new EventDTO();
            map = new HashMap<>();
            for (String o : op) {
                if (!"#".equals(o)) {
                    HashSet<String> endSet = new HashSet<>();
                    //遍历result赛选后的数据，判断是否带有选项o
                    for (String str : result) {
                        if (str.contains(o)) {
                            int index = str.lastIndexOf(",");
                            String end = str.substring(index + 1);
                            if (!"".equals(end)) {
                                endSet.add(end);
                            }
                        }
                    }
                    //System.out.println(endSet);
                    String[] endArr = new String[endSet.size()];
                    String[] cgTree = endSet.toArray(endArr);
                    map.put(o, cgTree);
                }
            }
            if (map.size() > 0) {
                event.setEventMap(map);
                eventDTOList.add(event);
            }

        }
        return JsonUtils.getJsonString(eventDTOList);
    }

    /**
     * 递归处理
     */
    private static void showDecisionMap(JSONObject map, StringBuilder sb, List<String> list) {
        Set<String> cg = map.keySet();
        for (String c : cg) {
            JSONObject object = map.getJSONObject(c);
            if (!object.getBoolean("final")) {
                sb.append(c).append(",");
                JSONObject featureDecisionMap = object.getJSONObject("featureDecisionMap");
                showDecisionMap(featureDecisionMap, sb, list);
                //递归回来需要清除上一次的
                //System.out.println(c + "需要清除？" + sb.toString());
                int index = sb.indexOf(c);
                if (-1 != index) {
                    sb.delete(index, sb.length());
                }
            } else {
                String result = object.getString("resultClassify");
                if (!"BAD".equals(result)) {
                    sb.append(c).append(",");
                    list.add(sb.toString() + result);
                }
            }
        }
    }


    /**
     * 构建决策树
     */
    private static List<Data> createDataList() {
        List<Data> dataList = new ArrayList<>();
        for (String end : ends) {
            String[] endData = end.split(",");
            String lastEnd = endData[endData.length - 1];
            HashMap<String, String> feature = new HashMap<>();
            for (int j = 0; j < endData.length - 1; j++) {
                feature.put(labels[j], endData[j]);
            }
            dataList.add(new Data(feature, lastEnd));
        }
        return dataList;
    }

    /**
     *  结局路线关键的CG个数统计
     *
     */
    private static void getKeyMap() {
        for (String end : play) {
            if (!"#".equals(end)) {
                Set<String> keySet = map.keySet();
                for (String op : keySet) {
                    String myEnd = map.get(op);
                    if (end.equals(myEnd)) {
                        if (!keyMap.containsKey(end)) {
                            keyMap.put(end, 1);
                        } else {
                            Integer count = keyMap.get(end);
                            keyMap.put(end, ++count);
                        }
                    }
                }
            }
        }
        System.out.println("每个结局路线关键的CG个数统计 --> " + keyMap);
        // 排序
        LinkedHashMap<String, Integer> map = sortDescend(keyMap);
        //然后重新修改play
        Set<String> keySet = map.keySet();
        play = new String[keySet.size() + 1];
        int count = 0;
        play[count] = "#";
        for (String key : keySet) {
            play[++count] = key;
        }
    }

    /**
     * Map的value值排序
     *
     */
    private static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortDescend(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        //降序
        //list.sort((o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));
        //升序
        list.sort(Comparator.comparing(o -> (o.getValue())));

        LinkedHashMap<K, V> returnMap = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            returnMap.put(entry.getKey(), entry.getValue());
        }
        return returnMap;
    }

    /**
     * 总共结局数
     *
     */
    private static Map<String, Integer> getTotalEnd(String options, Map<String, String> map) {
        LinkedHashMap<String, Integer> playMap = new LinkedHashMap<>();
        for (String end : play) {
            if (!"#".equals(end)) {
                Set<String> keySet = map.keySet();
                for (String op : keySet) {
                    String myEnd = map.get(op);
                    if (end.equals(myEnd)) {
                        if (!playMap.containsKey(end)) {
                            playMap.put(end, 0);
                        }
                    }
                }
            }
        }

        String[] split = options.split(",");
        for (String op : split) {
            String play = map.get(op);
            if (!"#".equals(play)) {
                if (playMap.containsKey(play)) {
                    Integer count = playMap.get(play);
                    playMap.put(play, ++count);
                }
            }
        }
        return playMap;
    }

    /**
     * CG分支事件
     *
     * @param op 选项
     */
    private static void cg(String op) {
        String[] opArr;

        switch (op) {
            case "分支1":
                List<String> op1 = Lists.newArrayList("A1", "B1", "C1");
                map.put("A1", "#");
                map.put("B1", "新仓");
                map.put("C1", "#");
                //重新初始化选项数组
                opArr = new String[op1.size()];
                opArr = op1.toArray(opArr);
                list.add(opArr);
                break;

            case "分支2":
                List<String> op2 = Lists.newArrayList("A2", "B2", "C2");
                map.put("A2", "#");
                map.put("B2", "叶恵");
                map.put("C2", "#");
                //重新初始化选项数组
                opArr = new String[op2.size()];
                opArr = op2.toArray(opArr);
                list.add(opArr);
                break;

            case "分支3":
                List<String> op3 = Lists.newArrayList("A3", "B3");
                map.put("A3", "乃华");
                map.put("B3", "#");
                //重新初始化选项数组
                opArr = new String[op3.size()];
                opArr = op3.toArray(opArr);
                list.add(opArr);
                break;

            case "分支4":
                List<String> op4 = Lists.newArrayList("A4", "B4", "C4");
                map.put("A4", "#");
                map.put("B4", "#");
                map.put("C4", "美由纪");
                //重新初始化选项数组
                opArr = new String[op4.size()];
                opArr = op4.toArray(opArr);
                list.add(opArr);
                break;

            case "分支5":
                List<String> op5 = Lists.newArrayList("A5", "B5");
                map.put("A5", "新仓");
                map.put("B5", "#");
                //重新初始化选项数组
                opArr = new String[op5.size()];
                opArr = op5.toArray(opArr);
                list.add(opArr);
                break;

            case "分支6":
                List<String> op6 = Lists.newArrayList("A6", "B6");
                map.put("A6", "乃华");
                map.put("B6", "美由纪");
                //重新初始化选项数组
                opArr = new String[op6.size()];
                opArr = op6.toArray(opArr);
                list.add(opArr);
                break;

            case "分支7":
                List<String> op7 = Lists.newArrayList("A7", "B7", "C7");
                map.put("A7", "乃华");
                map.put("B7", "美由纪");
                map.put("C7", "新仓");
                //重新初始化选项数组
                opArr = new String[op7.size()];
                opArr = op7.toArray(opArr);
                list.add(opArr);
                break;

            case "分支8":
                List<String> op8 = Lists.newArrayList("A8", "B8", "C8");
                map.put("A8", "叶恵");
                map.put("B8", "新仓");
                map.put("C8", "BAD");
                //重新初始化选项数组
                opArr = new String[op8.size()];
                opArr = op8.toArray(opArr);
                list.add(opArr);
                break;
        }
    }
}
